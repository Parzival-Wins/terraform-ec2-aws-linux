provider "aws" {
  shared_credentials_file  = "~/.aws/creds"
  profile                  = "getit"
  region                   = "${var.region}"

}

resource "aws_instance" "example" {
  ami           = "ami-0d729a60"
  instance_type = "t2.micro"

}
